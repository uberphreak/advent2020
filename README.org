#+title: Advent of Code 2020

* Introduction
  This respository contains code I've written to solve puzzles from
  [[https://adventofcode.com/2020][Advent of Code 2020]]. Last year, I took the opportunity to learn more
  about the differences between Python 2 and 3. This year, I decided
  to try Lisp alongside Python 3. I decided to add Lisp to the mix
  because Emacs is awesome. While I've played around enough to
  configure the editor (I even wrote an Elisp package to add COBOL
  support to =org-babel=!), I haven't done much else in Lisp. So far,
  Lisp has not disappointed. It's been incredibly exciting to solve
  AoC challenges in Lisp!
* Organization
  Admittedly, I'm not always the most organized. I will probably go
  back at some point and organize this a bit better. At the moment, I
  just have two =org-mode= files, one for Python and one for Lisp. I
  also added an OpenDocument Spreadsheet that I used to work out part
  2 of day 10, because the last thing I wanted to do was traverse a
  tree over a quadrillion times.
